
# Using UTM.app to run virtualized RDK-B

[UTM.app](https://mac.getutm.app/) can be used to run a RDK-B virtual machine 
on Apple Silicon (M-series SoC) hardware, providing native levels of performance.

## Requirements

* Apple Silicon (Arm64) Mac system.
* UTM.app installed (install from Mac App store or download from [mac.getutm.app](https://mac.getutm.app))

## How to setup RDK-B

### Creating the RDK-B VM instance

1. In UTM, click "+" to bring up the new VM start window
2. Choose **Virtualize**
3. Choose **Other**
4. Select **None** for **Boot Device**
5. For Hardware, 2048MiB (2GB) RAM and 2 CPU cores should be sufficient
6. For disk size, left the default size alone (we will choose a different disk later)
7. Do not setup a shared directory
8. Proceed with the creation of the VM

### Importing the RDK-B image and network configuration

Once the VM is in the left side menu, right click and scroll down to the bottom of the left hand pane.

1. Under "VirtIO Drive", click "new" and press the "Import" button
2. Select the `rdk.qcow` file you converted earlier

    Two "VirtIO Drive"'s will now appear on the left hand side. Delete the original entry (which would have a UUID-style filename)

3. For the rdk.qcow2 file, click "Resize" and resize it to at least 10GiB.

4. Under **Devices**, click "New" then "Network".

5. On the new network entry, change _Network Mode_ to _Shared Network_.

6. On the top (original) network entry, change the network mode to _Host Only_.

While not strictly required, you can delete the _Display_ device and replace it with a _Serial_ device to get a text-only console, without the overhead of the framebuffer.
