**Raspberry Pi instructions**

The `wic` image can be written directly to a microSD card and booted on a Raspberry Pi 4.

This boot flow uses U-Boot in ["standard boot"](https://docs.u-boot.org/en/latest/develop/bootstd/index.html#standard-boot)
to function as a system firmware which then loads the platform-agnostic bootloader (systemd-boot).

```
dd if=rdkb-generic-broadband-image_rdk-next_20240830003908.rootfs.wic of=/dev/sda bs=1M
```

By default, kernel bootup messages are sent to the serial console (pins 6,8,10* of the Raspberry Pi), but both serial and HDMI consoles are available for login. To see kernel bootup messages on the graphical console, edit the kernel command line to add `console=tty1`.

\* For more information on the Raspberry Pi serial UART, see [this article](https://www.jeffgeerling.com/blog/2021/attaching-raspberry-pis-serial-console-uart-debugging) for an introduction.

You will need one of these supported Ethernet devices to function as the "WAN" interface:
- CDC-NCM based devices (e.g UGREEN CR111, UGREEN CM475)
- Asix AX88179 based devices (some will run in CDC-NCM mode)
- PCIe Ethernet devices on CM4 board (like Realtek R8XXX)

For this proof of concept, the Raspberry Pi built-in WiFi is currently **not** supported.

The onboard interface on the RPI4 will be mapped to the LAN side (eth0 -> brlan0)

The WAN interface will be remapped as `eth6`:
```
$ ifconfig eth6
eth6      Link encap:Ethernet  HWaddr C8:A3:62:14:35:97
          inet addr:192.168.10.219  Bcast:192.168.10.255  Mask:255.255.255.0
          inet6 addr: fd1b:5548:66d1:0:caa3:62ff:fe14:3597/64 Scope:Global
          inet6 addr: fd1b:5548:66d1::21f/128 Scope:Global
          inet6 addr: fe80::caa3:62ff:fe14:3597/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:34 errors:0 dropped:0 overruns:0 frame:0
          TX packets:42 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:3834 (3.7 KiB)  TX bytes:4732 (4.6 KiB)
```