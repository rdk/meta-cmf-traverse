#!/bin/sh
##########################################################################
# If not stated otherwise in this file or this component's Licenses.txt
# file the following copyright and licenses apply:
#
# Copyright 2024 RDK Management
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##########################################################################

# ASIX USB 3.0 devices are disabled from loading at boot
# to prevent issues with some devices that are in CDC-NCM
# mode
modprobe ax88179_178a

MACHINE_NAME=$(strings "/proc/device-tree/compatible" | head -n 1)
if [ "${MACHINE_NAME}" != "traverse,ten64" ]; then
	# Emulate a Ten64
	if [ -d "/sys/class/net/eth1" ]; then
		ip link set eth1 down
		ip link set dev eth1 name eth6
	fi
	if [ -d "/sys/class/net/eth2" ]; then
		ip link set eth2 down
		ip link set dev eth2 name eth8
	fi
else
	logger "[ethwan_intf.sh] Unknown machine: ${MACHINE_NAME}"
fi

if [ -d "/sys/class/net/eth6" ]; then
	ip link set eth6 up
fi
if [ -d "/sys/class/net/eth8" ]; then
	ip link set eth8 up
fi
