require ccsp_common_genericarm.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
    file://0001-parodus-start_parodus-Implement-_PLATFORM_GENERICARM.patch;apply=no \
"

do_genericarm_patches() {
    cd ${S}
    if [ ! -e genericarm_patch_applied ]; then
        bbnote "Applying 0001-parodus-start_parodus-Implement-_PLATFORM_GENERICARM.patch into ${S}"
        patch -p1 -i ${WORKDIR}/0001-parodus-start_parodus-Implement-_PLATFORM_GENERICARM.patch
        touch genericarm_patch_applied
    fi
}
addtask genericarm_patches after do_unpack before do_compile