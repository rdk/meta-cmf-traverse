SRC_URI_append = " \
       git://gitlab.traverse.com.au/rdk/devices-genericarm-hal.git;protocol=https;branch=ten64-wan-manager;destsuffix=git/source/ethsw/devices_genericarm;name=ethswhal-genericarm \
"

SRCREV_ethswhal-genericarm = "d9321478d52e79b5cddd5594e24cd9b6864c075a"

DEPENDS += "libnl"

CFLAGS_append  = " ${@bb.utils.contains('DISTRO_FEATURES', 'rdkb_wan_manager', '-DFEATURE_RDKB_WAN_MANAGER', '', d)}"
CFLAGS_append  = " -I${STAGING_INCDIR}/libnl3"
LDFLAGS_append = " -lnl-3 -lnl-route-3"
do_configure_prepend(){
    ln -sf ${S}/devices_genericarm/source/hal-ethsw/ccsp_hal_ethsw.c ${S}/ccsp_hal_ethsw.c
}
