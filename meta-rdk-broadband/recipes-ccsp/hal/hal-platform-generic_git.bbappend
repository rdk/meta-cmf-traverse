SRC_URI_append = " \
       git://gitlab.traverse.com.au/rdk/devices-genericarm-hal.git;protocol=https;branch=ten64-wan-manager;destsuffix=git/source/platform/devices_genericarm;name=platformhal-genericarm \
"

SRCREV_platformhal-genericarm = "d9321478d52e79b5cddd5594e24cd9b6864c075a"

DEPENDS += "utopia-headers"
CFLAGS_append = " \
    -I=${includedir}/utctx \
"
CFLAGS_append_aarch64 = " -D_64BIT_ARCH_SUPPORT_"
do_configure_prepend(){
    rm ${S}/platform_hal.c
    ln -sf ${S}/devices_genericarm/source/platform/platform_hal.c ${S}/platform_hal.c
    rm ${S}/Makefile.am
    ln -sf ${S}/devices_genericarm/source/platform/Makefile.am ${S}/
}

