# Raspberry Pi bootstage for universal image

The `rpi-bootfiles` refer to the firmware a Raspberry Pi machine
loads from the SD card to configure the system
(referred to as BL0/BL1), to hand off to the bootloader (BL3).

These files are imported from [meta-raspberrypi](https://git.yoctoproject.org/meta-raspberrypi). Due to the different machine setups for the
Universal Arm image and Raspberry Pi, meta-cmf-traverse cannot
use the rpi-bootfiles from meta-raspberrypi directly.

To prevent our (`meta-cmf-traverse`) version of rpi-bootfiles
from conflicting with a `meta-raspberrypi`, Bitbake's
[`BBFILES_DYNAMIC`](https://docs.yoctoproject.org/bitbake/dev/bitbake-user-manual/bitbake-user-manual-ref-variables.html#term-BBFILES_DYNAMIC)
mechanism is used to prevent simultaneous inclusion of meta-raspberrypi.

See [layer.conf](https://github.com/rdkcentral/meta-cmf-traverse/blob/72006b3882b1a728fd2d2ea65dc0e80bf184ef51/conf/layer.conf#L10) for the relevant configuration:
```
BBFILES_DYNAMIC += "\
    !raspberrypi:${LAYERDIR}/dynamic-layers/raspberrypi/recipes-*/*/*.bbappend \
    !raspberrypi:${LAYERDIR}/dynamic-layers/raspberrypi/recipes-*/*/*.bb \
"
```
