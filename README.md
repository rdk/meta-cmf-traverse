# Meta layer for RDK-B for Traverse and standardized-Arm boards

This meta layer provides the components to support RDK-B on the
[Traverse Technologies Ten64](https://www.traverse.com.au/hardware/ten64) (NXP LS1088A)
platform. Additionally, the same image functions (on a "best-effort" support basis)
on virtual machines and hardware compatible with the [Arm SystemReady](https://www.arm.com/architecture/system-architectures/systemready-certification-program)
specifications.

These compatible machines include common emulation/virtualization packages (QEMU, virt-manager, UTM.app),
as well as platforms where an EFI-capable bootloader is available. The images (wics) produced
by this build are also compatible with the Raspberry Pi 4 and derivatives.

## How to use

There are some RDK-B packages for which custom branches are needed, so you will need to use
our `rdkb-extsrc-traverse.xml` manifest:

```
repo init -u 'https://gitlab.traverse.com.au/rdk/manifests.git' -m rdkb-extsrc-traverse.xml -b "traverse/universalarm" < /dev/null
```

You can follow the standard build process for RDK-B like so:

```
repo sync
MACHINE=ten64-rdk-broadband . ./meta-cmf-traverse/setup-environment
bitbake rdk-generic-broadband-image
```

The completed images will be available under `build-ten64-rdk-broadband/tmp/deploy/images/ten64-rdk-broadband`.

The most interesting output file is the `wic` file, normally called `rdk-generic-broadband-image-ten64-rdk-broadband.wic`:

```
$ ls -la build-ten64-rdk-broadband/tmp/deploy/images/ten64-rdk-broadband/rdk-generic-broadband-image-ten64-rdk-broadband.wic
lrwxrwxrwx 2 matt matt 63 Sep 19 12:14 build-ten64-rdk-broadband/tmp/deploy/images/ten64-rdk-broadband/rdk-generic-broadband-image-ten64-rdk-broadband.wic -> rdkb-generic-broadband-image_rdk-next_20240919020946.rootfs.wic

$ du -h $(readlink -f build-ten64-rdk-broadband/tmp/deploy/images/ten64-rdk-broadband/rdk-generic-broadband-image-ten64-rdk-broadband.wic)
1.4G    build-ten64-rdk-broadband/tmp/deploy/images/ten64-rdk-broadband/rdkb-generic-broadband-image_rdk-next_20240919020946.rootfs.wic
```

# Running the generated image

Please see the [doc](doc/) folder for information for other VM software or boards, including:

* virt-manager
* UTM.app
* Raspberry Pi 4

Please note, only Mediatek PCIe WiFi solutions (MT7915/MT7916) are supported at this time.

## QEMU (Arm64 virtualized or emulated)
You will need an EFI  or U-Boot 'BIOS' binary, such as from `qemu-efi-aarch64`
in Debian (EDK2), [retrage/edk2-nightly](https://retrage.github.io/edk2-nightly/).
or U-Boot's qemu aarch64 target (you can find a prebuilt binary [here](http://downloads.openwrt.org/releases/23.05.4/targets/armsr/armv8/u-boot-qemu_armv8/) ).

On virtualization capable Arm64 hosts (Ten64, AWS a1.metal) you can use KVM for near-native performance.

You can invoke QEMU like this:

```
# Convert the WIC image to qcow2. This is optional but allows you
# to use features like snapshotting.
unxz rdk.img.xz
qemu-img convert -O qcow2 rdk.img rdk.qcow2

# It can be helpful to create a snapshot to quickly revert
# any changes that are made
qemu-img create -f qcow2 -b rdk.qcow2 -F qcow2 rdk.resize.qcow2
# And resize that snapshot to allow space for upgrades
qemu-img resize rdk.resize.qcow2 10G

DISK=${DISK:-rdk.resize.qcow2}
qemu-system-aarch64 -nographic \
    -cpu host --enable-kvm -machine virt \
    -bios u-boot.bin \
    -smp 2 -m 1024 \
    -device virtio-rng-pci \
    -hda "${DISK}" \
    -netdev tap,id=testlan -net nic,netdev=testlan \
    -netdev tap,id=testlan2 -net nic,netdev=testlan2 \
    -netdev user,id=testwan -net nic,netdev=testwan \
    -device pci-serial
```

On other host platforms, replace `-cpu host --enable-kvm` with `-cpu cortex-a53`.

## Ten64

Use the [recovery environment](https://ten64doc.traverse.com.au/quickstart/#recovery-environment) to
stage and write the image to disk.

To speed up the process, we recommend converting the .img.xz from the build to qcow2, then transferring
to the Ten64-Recovery over SSH/SCP.

```
# On workstation
unxz rdk.img.xz
qemu-img convert -O qcow2 -C rdk.img rdk.qcow2
scp rdk.qcow2 root@192.168.10.1:/tmp

# On Ten64
blkdiscard -f /dev/nvme0n1
write-image-disk rdk.qcow2 /dev/nvme0n1
reboot
```
