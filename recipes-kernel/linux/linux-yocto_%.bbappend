FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://proc-event.cfg"

SRC_URI_append = "file://0001-REFPLTB-1735-Port-triggering-Feature-Integration.patch \
                  file://0002-REFPLTB-2763-Kirkstone-Kernel-5.15-up-step-RDK-B-RPI.patch \
                  file://rpi-support.cfg \
                  file://framebuffer.cfg \
"

# Patches required to allow backported mt76 driver
SRC_URI_append_kirkstone = " file://0003-mtk-backports-for-5-15.patch \
                             file://0004-net-allow-PAGE_POOL-to-be-user-selected.patch \
"

SRC_URI_append_broadband = " \
                             file://remove_unused_modules.cfg \
                             file://rdkb.cfg \
                             file://rdkb-acm.cfg \
                             file://netfilter.cfg  \
                             file://no-use-kernel-wifi.cfg \
"
SRC_URI_append_extender = " file://remove_unused_modules.cfg"
SRC_URI_append_extender = " \
                            file://rdkb.cfg \
                            file://rdkb-ext.cfg \
                            file://regdb.patch \
"

SRC_URI_remove = " file://0001-add-support-for-http-host-headers-cookie-url-netfilt.patch "
SRC_URI_remove_broadband =  " ${@bb.utils.contains("MACHINE_FEATURES", "vc4graphics", "file://vc4graphics.cfg", "", d)}"

do_install_append() {
    install -d ${D}${includedir}
    install -m 0644 ${B}/include/generated/autoconf.h ${D}${includedir}/autoconf.h
}

sysroot_stage_all_append () {
    install -d ${SYSROOT_DESTDIR}${includedir}
    install -m 0644 ${D}${includedir}/autoconf.h ${SYSROOT_DESTDIR}${includedir}/autoconf.h
}

do_deploy_append_refApp () {
    sed -i '1 s|$|vt.global_cursor_default=0|' ${DEPLOYDIR}/bcm2835-bootfiles/cmdline.txt
}

do_deploy_append_hybrid () {
    do_deploy_config
}

do_deploy_append_client () {
    do_deploy_config
}

do_deploy_append_ipclient () {
    do_deploy_config
}

do_deploy_config () {
}

PACKAGES += "kernel-autoconf"
PROVIDES += "kernel-autoconf"

FILES_kernel-autoconf = "${includedir}/autoconf.h"
